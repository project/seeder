<?php

namespace Drupal\seeder;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining the Seeder Profile entity.
 */
interface SeederProfileInterface extends ConfigEntityInterface {

  /**
   * Get the Data Samplers.
   *
   * @return array
   *   An array of data sampler id keys.
   */
  public function getDataSamplers(): array;

  /**
   * Set the Data Samplers.
   *
   * @param array $data_samplers
   *   An array of data sampler id keys.
   *
   * @return \Drupal\seeder\SeederProfileInterface
   */
  public function setDataSamplers(array $data_samplers): SeederProfileInterface;

  /**
   * @return array
   */
  public function getBundles(): array;

  /**
   * @param array $bundles
   *
   * @return \Drupal\seeder\SeederProfileInterface
   */
  public function setBundles(array $bundles): SeederProfileInterface;

}
