<?php

namespace Drupal\seeder;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Plugin\PluginBase;

/**
 * Class SeederDataSamplerBase.
 *
 * @package Drupal\seeder
 */
abstract class SeederDataSamplerBase extends PluginBase implements SeederDataSamplerInterface {

  /**
   * Field Definition.
   *
   * @var \Drupal\Core\Field\FieldDefinitionInterface
   */
  protected $fieldDefinition;

  /**
   * Lang code.
   *
   * @var string
   */
  protected $locale;

  /**
   * {@inheritdoc}
   */
  public function getFieldTypeIds() {
    $plugin_definition = $this->getPluginDefinition();
    if (isset($plugin_definition['field_type_ids']) && $plugin_definition($plugin_definition['field_type_ids'])) {
      return $plugin_definition['field_type_ids'];
    }
    return [];
  }

  /**
   * @param \Drupal\Core\Field\FieldDefinitionInterface $fieldDefinition
   *
   * @return $this
   */
  public function setFieldDefinition(FieldDefinitionInterface $fieldDefinition): SeederDataSamplerInterface {
    $this->fieldDefinition = $fieldDefinition;
    return $this;
  }

  /**
   * @param string $locale
   *
   * @return $this
   */
  public function setLocale($locale): SeederDataSamplerInterface {
    $this->locale = $locale;
    return $this;
  }

  public function getLocale(): string {
    return $this->locale;
  }

}
