<?php

namespace Drupal\seeder\Plugin\SeederDataSampler;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\seeder\SeederDataSamplerBase;
use Faker\Factory;

/**
 * Class CountryCodeFakerDataSampler.
 *
 * @SeederDataSampler(
 *   id = "faker_country_code",
 *   label = @Translation("Faker Country Code"),
 *   field_type_ids = {
 *     "address_country",
 *   }
 * )
 */
class CountryCodeFakerDataSampler extends SeederDataSamplerBase {

  /**
   * {@inheritdoc}
   */
  public function generateSeederValue(FieldDefinitionInterface $field_definition = NULL) {
    return [
      'value' => Factory::create($this->getLocale())->countryCode,
    ];
  }

}
