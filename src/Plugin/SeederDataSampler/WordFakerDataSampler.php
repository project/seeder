<?php

namespace Drupal\seeder\Plugin\SeederDataSampler;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\seeder\SeederDataSamplerBase;
use Faker\Factory;

/**
 * Class WordFakerDataSampler.
 *
 * @SeederDataSampler(
 *   id = "faker_word",
 *   label = @Translation("Faker Word"),
 *   field_type_ids = {
 *     "string",
 *   }
 * )
 */
class WordFakerDataSampler extends SeederDataSamplerBase {

  /**
   * {@inheritdoc}
   */
  public function generateSeederValue(FieldDefinitionInterface $field_definition = NULL) {
    return [
      'value' => ucfirst(Factory::create($this->getLocale())->word),
    ];
  }

}
