<?php

namespace Drupal\seeder\Plugin\SeederDataSampler;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\seeder\SeederDataSamplerBase;
use Faker\Factory;

/**
 * Class TextWithSummaryFakerDataSampler.
 *
 * @SeederDataSampler(
 *   id = "faker_text_with_summary",
 *   label = @Translation("Faker Text (formatted, long, with summary)"),
 *   field_type_ids = {
 *     "text_with_summary",
 *   }
 * )
 */
class TextWithSummaryFakerDataSampler extends TextFakerDataSampler {

  /**
   * {@inheritdoc}
   * @throws \Exception
   */
  public function generateSeederValue(FieldDefinitionInterface $field_definition = NULL) {
    $values = parent::generateSeederValue($field_definition, $this->getLocale());
    $values['summary'] = $values['value'];
    return $values;
  }

}
