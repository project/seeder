<?php

namespace Drupal\seeder\Plugin\SeederDataSampler;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\seeder\SeederDataSamplerBase;
use Faker\Factory;

/**
 * Class CountryFakerDataSampler.
 *
 * @SeederDataSampler(
 *   id = "faker_country",
 *   label = @Translation("Faker Country"),
 *   field_type_ids = {
 *     "string",
 *
 *   }
 * )
 */
class CountryFakerDataSampler extends SeederDataSamplerBase {

  /**
   * {@inheritdoc}
   */
  public function generateSeederValue(FieldDefinitionInterface $field_definition = NULL) {
    return [
      'value' => Factory::create($this->getLocale())->country,
    ];
  }

}
