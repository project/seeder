<?php

namespace Drupal\seeder\Plugin\SeederDataSampler;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\seeder\SeederDataSamplerBase;
use Faker\Factory;

/**
 * Class TextWithSummaryFakerDataSampler.
 *
 * @SeederDataSampler(
 *   id = "faker_text_with_summary",
 *   label = @Translation("Faker Text (formatted, long, with summary)"),
 *   field_type_ids = {
 *     "text",
 *     "text_long",
 *     "string_long"
 *   }
 * )
 */
class TextFakerDataSampler extends SeederDataSamplerBase {

  /**
   * {@inheritdoc}
   * @throws \Exception
   */
  public function generateSeederValue(FieldDefinitionInterface $field_definition = NULL) {

    $faker = Factory::create($this->getLocale());
    $settings = $field_definition->getSettings();

    if (empty($settings['max_length'])) {
      $value = $faker->paragraphs(12, TRUE);
    }
    else {
      // Textfield handling.
      $value = substr($faker->paragraph(random_int(1, $settings['max_length'] / 3)), 0, $settings['max_length']);
    }

    if ($field_definition->getType() === 'string_long') {
      return [
        'value' => $value,
      ];
    }
    return [
      'value' => $value,
      'format' => filter_fallback_format(),
    ];
  }

}
