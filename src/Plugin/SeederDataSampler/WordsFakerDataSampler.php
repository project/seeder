<?php

namespace Drupal\seeder\Plugin\SeederDataSampler;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\seeder\SeederDataSamplerBase;
use Faker\Factory;

/**
 * Class WordsFakerDataSampler.
 *
 * @SeederDataSampler(
 *   id = "faker_words",
 *   label = @Translation("Faker Words"),
 *   field_type_ids = {
 *     "string",
 *   }
 * )
 */
class WordsFakerDataSampler extends SeederDataSamplerBase {

  /**
   * {@inheritdoc}
   */
  public function generateSeederValue(FieldDefinitionInterface $field_definition = NULL) {
    return [
      'value' => ucfirst(Factory::create($this->getLocale())->words(4, TRUE)),
    ];
  }

}
