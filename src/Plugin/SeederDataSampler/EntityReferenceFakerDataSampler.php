<?php

namespace Drupal\seeder\Plugin\SeederDataSampler;

use Drupal;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\seeder\SeederDataSamplerBase;

/**
 * Class EntityReferenceFakerDataSampler.
 *
 * @SeederDataSampler(
 *   id = "faker_country_code",
 *   label = @Translation("Entity reference"),
 *   field_type_ids = {
 *     "entity_reference",
 *   }
 * )
 */
class EntityReferenceFakerDataSampler extends SeederDataSamplerBase {

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function generateSeederValue(FieldDefinitionInterface $field_definition = NULL) {
    $target_id = $this->getRandomAvailableEntity($field_definition);

    return [
      'target_id' => $target_id,
    ];
  }

  /**
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *
   * @return bool|mixed
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  private function getRandomAvailableEntity(FieldDefinitionInterface $field_definition) {
    $manager = Drupal::service('plugin.manager.entity_reference_selection');

    // Instead of calling $manager->getSelectionHandler($field_definition)
    // replicate the behavior to be able to override the sorting settings.
    $options = ([
        'target_type' => $field_definition->getFieldStorageDefinition()
          ->getSetting('target_type'),
        'handler' => $field_definition->getSetting('handler'),
        'entity' => NULL,
      ] + $field_definition->getSetting('handler_settings')) ?: [];

    /** @var \Drupal\Core\Entity\EntityTypeInterface $entity_type */
    $entity_type = Drupal::entityTypeManager()->getDefinition($options['target_type']);

    if (!$entity_type) {
      return FALSE;
    }

    $options['sort'] = [
      'field' => $entity_type->getKey('id'),
      'direction' => 'DESC',
    ];
    /** @var \Drupal\Core\Entity\EntityReferenceSelection\SelectionPluginBase $selection_handler */
    $selection_handler = $manager->getInstance($options);

    // Select a random number of references between the last 50 referenceable
    // entities created.
    $references = $selection_handler->getReferenceableEntities(NULL, 'CONTAINS', 1);
    if (!$references) {
      return FALSE;
    }
    $targetIds = reset($references);
    return reset($targetIds);
  }
}
