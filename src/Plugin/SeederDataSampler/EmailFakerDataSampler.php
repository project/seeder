<?php

namespace Drupal\seeder\Plugin\SeederDataSampler;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\seeder\SeederDataSamplerBase;
use Faker\Factory;

/**
 * Class EmailFakerDataSampler.
 *
 * @SeederDataSampler(
 *   id = "faker_email",
 *   label = @Translation("Faker Email"),
 *   field_type_ids = {
 *     "email",
 *     "string",
 *   }
 * )
 */
class EmailFakerDataSampler extends SeederDataSamplerBase {

  /**
   * {@inheritdoc}
   */
  public function generateSeederValue(FieldDefinitionInterface $field_definition = NULL) {
    return [
      'value' => Factory::create($this->getLocale())->safeEmail,
    ];
  }

}
