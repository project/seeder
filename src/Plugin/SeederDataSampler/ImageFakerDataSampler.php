<?php

namespace Drupal\seeder\Plugin\SeederDataSampler;

use Drupal\Component\Render\PlainTextOutput;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\File\Exception\FileException;
use Drupal\Core\File\FileSystemInterface;
use Drupal\seeder\SeederDataSamplerBase;
use Drupal\file\Entity\File;
use Faker\Factory;
use Faker\Generator;

/**
 * Class ImageFakerDataSampler.
 *
 * @SeederDataSampler(
 *   id = "faker_image",
 *   label = @Translation("Faker Image"),
 *   field_type_ids = {
 *     "image",
 *   }
 * )
 */
class ImageFakerDataSampler extends SeederDataSamplerBase {

  /**
   * {@inheritdoc}
   * @throws \Exception
   */
  public function generateSeederValue(FieldDefinitionInterface $field_definition = NULL) {
    $faker = Factory::create($this->getLocale());
    $settings = $field_definition->getSettings();
    static $images = [];

    $min_resolution = empty($settings['min_resolution']) ? '1920X1080' : $settings['min_resolution'];
    $max_resolution = empty($settings['max_resolution']) ? '2500x1406' : $settings['max_resolution'];
    $extensions = array_intersect(explode(' ', $settings['file_extensions']), ['png', 'gif', 'jpg', 'jpeg']);
    $extension = array_rand(array_combine($extensions, $extensions));

    // Generate a max of 5 different images.
    /** @var \Drupal\Core\File\FileSystemInterface $file_system */
    $file_system = \Drupal::service('file_system');
    $tmp_file = $file_system->tempnam('temporary://', 'generateImage_');
    $destination = $tmp_file . '.' . $extension;
    try {
      $file_system->move($tmp_file, $destination);
    }
    catch (FileException $e) {
      // Ignore failed move.
    }

    if ($path = self::image($file_system->realpath($destination), $min_resolution, $max_resolution, $faker)) {
      /** @var \Drupal\file\FileInterface $image */
      $image = File::create();
      $image->setFileUri($path);
      $image->setOwnerId(\Drupal::currentUser()->id());
      $image->setMimeType(\Drupal::service('file.mime_type.guesser')->guess($path));
      $image->setFileName($file_system->basename($path));
      $destination_dir = self::doGetUploadLocation($settings);
      $file_system->prepareDirectory($destination_dir, FileSystemInterface::CREATE_DIRECTORY);
      $destination = $destination_dir . '/' . basename($path);
      $file = file_move($image, $destination);
      $images[$extension][$min_resolution][$max_resolution][$file->id()] = $file;
    }
    else {
      return [];
    }

    list($width, $height) = getimagesize($file->getFileUri());

    return [
      'target_id' => $file->id(),
      'alt' => $faker->sentence(),
      'title' => $faker->sentence(),
      'width' => $width,
      'height' => $height,
    ];
  }

  /**
   * Create a placeholder image.
   *
   * @param $destination
   * @param $min_resolution
   * @param $max_resolution
   * @param \Faker\Generator $faker
   *
   * @return bool|\RuntimeException|string
   * @throws \Exception
   */
  private static function image($destination, $min_resolution, $max_resolution, Generator $faker) {
    $min = explode('x', $min_resolution);
    $max = explode('x', $max_resolution);

    $width = random_int((int) $min[0], (int) $max[0]);
    $height = random_int((int) $min[1], (int) $max[1]);

    return $faker->image($destination, $width, $height, 'cats', TRUE, TRUE, 'Faker');
  }

  /**
   * Determines the URI for a file field.
   *
   * @param array $settings
   *   The array of field settings.
   * @param array $data
   *   An array of token objects to pass to Token::replace().
   *
   * @return string
   *   An unsanitized file directory URI with tokens replaced. The result of
   *   the token replacement is then converted to plain text and returned.
   *
   * @see \Drupal\Core\Utility\Token::replace()
   */
  protected static function doGetUploadLocation(array $settings, $data = []) {
    $destination = trim($settings['file_directory'], '/');

    // Replace tokens. As the tokens might contain HTML we convert it to plain
    // text.
    $destination = PlainTextOutput::renderFromHtml(\Drupal::token()->replace($destination, $data));
    return $settings['uri_scheme'] . '://' . $destination;
  }

}
