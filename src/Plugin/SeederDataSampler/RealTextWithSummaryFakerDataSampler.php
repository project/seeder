<?php

namespace Drupal\seeder\Plugin\SeederDataSampler;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\seeder\SeederDataSamplerBase;
use Faker\Factory;

/**
 * Class RealTextWithSummaryFakerDataSampler.
 *
 * @SeederDataSampler(
 *   id = "faker_real_text_with_summary",
 *   label = @Translation("Faker Real Text (formatted, long, with summary)"),
 *   field_type_ids = {
 *     "text_with_summary",
 *   }
 * )
 */
class RealTextWithSummaryFakerDataSampler extends RealTextFakerDataSampler {

  /**
   * {@inheritdoc}
   *
   * @throws \Exception
   */
  public function generateSeederValue(FieldDefinitionInterface $field_definition = NULL) {
    $values = parent::generateSeederValue($field_definition, $this->getLocale());
    $values['summary'] = $values['value'];
    return $values;
  }

}
