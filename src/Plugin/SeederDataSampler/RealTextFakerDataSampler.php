<?php

namespace Drupal\seeder\Plugin\SeederDataSampler;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\seeder\SeederDataSamplerBase;
use Faker\Factory;

/**
 * Class RealTextWithSummaryFakerDataSampler.
 *
 * @SeederDataSampler(
 *   id = "faker_real_text",
 *   label = @Translation("Faker Real Text (formatted)"),
 *   field_type_ids = {
 *     "text",
 *     "text_long",
 *     "string_long"
 *   }
 * )
 */
class RealTextFakerDataSampler extends SeederDataSamplerBase {

  /**
   * {@inheritdoc}
   *
   * @throws \Exception
   */
  public function generateSeederValue(FieldDefinitionInterface $field_definition = NULL) {

    $faker = Factory::create($this->getLocale());
    $settings = $field_definition->getSettings();

    // Run garbage collector to reduce memory exhaustion.
    gc_collect_cycles();

    if (empty($settings['max_length'])) {
      $value = $faker->realText(600);
    }
    else {
      // Textfield handling.
      $value = substr($faker->realText(random_int(1, $settings['max_length'] / 3)), 0, $settings['max_length']);
    }

    if ($field_definition->getType() === 'string_long') {
      return [
        'value' => $value,
      ];
    }
    return [
      'value' => $value,
      'format' => filter_fallback_format(),
    ];
  }

}
