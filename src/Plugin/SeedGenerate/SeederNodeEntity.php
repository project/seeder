<?php

namespace Drupal\seeder\Plugin\DevelGenerate;

use Drupal\devel_generate\Plugin\DevelGenerate\SeederEntityBase;

/**
 * Class SeederNodeEntityDevelGenerate.
 *
 * @package Drupal\seeder\Plugin\DevelGenerate
 */
class SeederNodeEntity extends SeederEntityBase {

  /**
   * {@inheritdoc}
   */
  protected function getEntityTypeId(): string {
    return 'node';
  }

}
