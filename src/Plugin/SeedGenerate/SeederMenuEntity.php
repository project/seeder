<?php

namespace Drupal\seeder\Plugin\DevelGenerate;

use Drupal\devel_generate\Plugin\DevelGenerate\UserDevelGenerate;

/**
 * Class SeederMenuEntitySeeder.
 *
 * @package Drupal\seeder\Plugin\DevelGenerate
 */
class SeederMenuEntity extends UserDevelGenerate {

  /**
   * {@inheritdoc}
   */
  protected function getEntityTypeId(): string {
    return 'menu_link_content';
  }

}
