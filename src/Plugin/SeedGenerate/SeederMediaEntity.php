<?php

namespace Drupal\seeder\Plugin\DevelGenerate;

use Drupal\devel_generate\Plugin\DevelGenerate\SeederEntityBase;

/**
 * Class SeederMediaEntityDevelGenerate.
 *
 * @package Drupal\seeder\Plugin\DevelGenerate
 */
class SeederMediaEntity extends SeederEntityBase {

  /**
   * {@inheritdoc}
   */
  protected function getEntityTypeId(): string {
    return 'media';
  }

}
