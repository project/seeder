<?php

namespace Drupal\seeder\Plugin\SeedGenerate;

use Drupal\Component\Utility\Random;
use Drupal\Core\Batch\BatchBuilder;
use Drupal\Core\Database\Connection;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\PluginBase;
use Drupal\Core\Routing\UrlGeneratorInterface;
use Drupal\seeder\SeederConstants;
use Drupal\seeder\SeederEntityGenerator;
use Exception;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class SeederEntityBase
 *
 * @package Drupal\seeder\Plugin\SeedGenerate
 */
abstract class SeederEntityBase extends PluginBase implements ContainerFactoryPluginInterface {

  /**
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $entityTypeStorage;

  /**
   * The entity storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $entityStorage;

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @var \Drupal\seeder\SeederEntityGenerator
   */
  protected $seederEntityGenerator;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * The url generator service.
   *
   * @var \Drupal\Core\Routing\UrlGeneratorInterface
   */
  protected $urlGenerator;

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * Provides system time.
   *
   * @var \Drupal\Component\Datetime\Time
   */
  protected $time;

  /**
   * Database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The plugin settings.
   *
   * @var array
   */
  protected $settings = [];

  /**
   * The random data generator.
   *
   * @var \Drupal\Component\Utility\Random
   */
  protected $random;

  /**
   * The construct.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param array $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\seeder\SeederEntityGenerator $seederEntityGenerator
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\Core\Routing\UrlGeneratorInterface $url_generator
   *   The url generator service.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   Provides system time.
   * @param \Drupal\Core\Database\Connection $database
   *   Database connection.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(array $configuration, $plugin_id, array $plugin_definition, SeederEntityGenerator $seederEntityGenerator, ModuleHandlerInterface $module_handler, UrlGeneratorInterface $url_generator, DateFormatterInterface $date_formatter, Connection $database) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->moduleHandler = $module_handler;
    $this->seederEntityGenerator = $seederEntityGenerator;
    $this->entityTypeManager = $this->seederEntityGenerator->getEntityTypeManager();
    $this->entityStorage = $this->getEntityTypeManager()->getStorage($this->getEntityTypeId());
    $bundleEntityTypeId = $this->entityStorage->getEntityType()->getBundleEntityType();
    $this->entityTypeStorage = $this->getEntityTypeManager()->getStorage($bundleEntityTypeId);
    $this->languageManager = $this->seederEntityGenerator->getLanguageManager();
    $this->urlGenerator = $url_generator;
    $this->dateFormatter = $date_formatter;
    $this->database = $database;
    $this->seederEntityGenerator = $seederEntityGenerator;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration, $plugin_id, $plugin_definition,
      $container->get('seeder.entity_generator'),
      $container->get('module_handler'),
      $container->get('url_generator'),
      $container->get('date.formatter'),
      $container->get('database')
    );
  }

  /**
   * Get the entity type id.
   *
   * @return string
   */
  protected function getEntityTypeId(): string {
    return $this->pluginDefinition['entity_type'];
  }

  /**
   * Returns the form for the plugin.
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return array
   *   The array of default setting values, keyed by setting names.
   */
  public function settingsForm(array $form, FormStateInterface $form_state): array {
    $bundles = $this->entityTypeStorage->loadMultiple();

    $profile_options = $locale_options = $title_options = [
      '_none_' => t('None'),
    ];

    //    if (empty($types)) {
    //      $create_url = $this->urlGenerator->generateFromRoute('node.type_add');
    //      $this->setMessage($this->t('You do not have any content types that can be generated. <a href=":create-type">Go create a new content type</a>', [':create-type' => $create_url]), 'error');
    //      return $form;
    //    }

    $options = [];

    foreach ($bundles as $bundle) {
      $options[$bundle->id()] = [
        'type' => ['#markup' => $bundle->label()],
      ];
    }

    $header = [
      'type' => $this->t('Entity'),
    ];

    $form[SeederConstants::BUNDLES] = [
      '#type' => 'tableselect',
      '#header' => $header,
      '#options' => $options,
      '#required' => TRUE,
    ];

    $form[SeederConstants::CLEAN] = [
      '#type' => 'checkbox',
      '#title' => $this->t('<strong>Delete all entities</strong> in these types before generating new content.'),
      '#default_value' => $this->getSetting(SeederConstants::CLEAN),
    ];
    $form[SeederConstants::LIMIT] = [
      '#type' => 'number',
      '#title' => $this->t('How many entities would you like to generate?'),
      '#default_value' => $this->getSetting(SeederConstants::LIMIT),
      '#required' => TRUE,
      '#min' => 1,
    ];

    $options = [1 => $this->t('Now')];
    foreach ([3600, 86400, 604800, 2592000, 31536000] as $interval) {
      $options[$interval] = $this->dateFormatter->formatInterval($interval, 1) . ' ' . $this->t('ago');
    }
    $form[SeederConstants::TIME_RANGE] = [
      '#type' => 'select',
      '#title' => $this->t('How far back in time should the entities be dated?'),
      '#description' => $this->t('Entity creation dates will be distributed randomly from the current time, back to the selected time.'),
      '#options' => $options,
      '#default_value' => 604800,
    ];

    $seederSamplerDefinitions = $this->seederEntityGenerator->getSeederDataSamplerManager()->getDefinitions();
    foreach ($seederSamplerDefinitions as $seederSamplerId => $seederSamplerDefinition) {
      if (in_array('string', $seederSamplerDefinition['field_type_ids'], TRUE)) {
        $title_options[$seederSamplerId] = $seederSamplerDefinition['label'];
      }
    }

    $form[SeederConstants::ENTITY_TITLE] = [
      '#type' => 'select',
      '#title' => $this->t('Name/title'),
      '#description' => $this->t('Use a Data Sampler for the media name.'),
      '#options' => $title_options,
    ];

    $form[SeederConstants::NAME_LENGTH] = [
      '#type' => 'number',
      '#title' => $this->t('Maximum number of words in titles'),
      '#default_value' => $this->getSetting(SeederConstants::NAME_LENGTH),
      '#required' => TRUE,
      '#min' => 1,
      '#max' => 255,
    ];

    //    $form['add_alias'] = [
    //      '#type' => 'checkbox',
    //      '#disabled' => !$this->moduleHandler->moduleExists('path'),
    //      '#description' => $this->t('Requires path.module'),
    //      '#title' => $this->t('Add an url alias for each entity.'),
    //      '#default_value' => FALSE,
    //    ];

    // Add the language and translation options.
    $form += $this->getLanguageForm('nodes');

    try {
      $entities = $this->getEntityTypeManager()->getStorage('seeder_profile')->loadMultiple();
    } catch (Exception $e) {
      $entities = [];
    }

    foreach ($entities as $key => $entity) {
      $profile_options[$key] = $entity->label();
    }
    $form[SeederConstants::PROFILE] = [
      '#title' => $this->t('Seeder Profile'),
      '#description' => $this->t('Use a Seeder Profile for content population.'),
      '#type' => 'select',
      '#required' => TRUE,
      '#options' => $profile_options,
    ];

    $form[SeederConstants::LOCALE] = [
      '#title' => $this->t('Locale'),
      '#description' => $this->t('Use a Locale for content population.'),
      '#type' => 'select',
      '#options' => array_merge($locale_options, array_combine(SeederConstants::LOCALES, SeederConstants::LOCALES)),
    ];

    $form['#redirect'] = FALSE;

    return $form;
  }

  /**
   * Execute the instructions in common for all SeedGenerate plugin.
   *
   * @param array $values
   *   The input values from the settings form.
   *
   */
  public function generate(array $values): void {
    $batch_builder = (new BatchBuilder())
      ->setTitle($this->t('Seed new entities...'))
      ->setFinishCallback([$this->seederEntityGenerator, 'finishProcess']);

    // Add the kill operation.
    if ($values[SeederConstants::CLEAN]) {
      $batch_builder->addOperation([$this->seederEntityGenerator, 'cleanUpEntitiesOperation'], [
          $this->getEntityTypeId(),
          $values,
        ]
      );
    }

    $batch_builder->addOperation([$this->seederEntityGenerator, 'createEntitiesOperation'], [
      $this->getEntityTypeId(),
      $values,
    ]);
    batch_set($batch_builder->toArray());
  }

  /**
   * Gets the entity type manager service.
   *
   * @return \Drupal\Core\Entity\EntityTypeManagerInterface
   *   The entity type manager service.
   */
  protected function getEntityTypeManager(): EntityTypeManagerInterface {
    return $this->entityTypeManager;
  }

  /**
   * Returns the array of settings, including defaults for missing settings.
   *
   * @param $key
   *
   * @return array
   *   The array of settings.
   */
  public function getSetting($key): ?array {
    // Merge defaults if we have no value for the key.
    if (!array_key_exists($key, $this->settings)) {
      $this->settings = $this->getDefaultSettings();
    }
    return $this->settings[$key] ?? NULL;
  }

  /**
   * Returns the default settings for the plugin.
   *
   * @return array
   *   The array of default setting values, keyed by setting names.
   */
  public function getDefaultSettings(): array {
    $definition = $this->getPluginDefinition();
    return $definition['settings'];
  }

  /**
   * Returns the current settings for the plugin.
   *
   * @return array
   *   The array of current setting values, keyed by setting names.
   */
  public function getSettings(): array {
    return $this->settings;
  }

  /**
 * Creates the language and translation section of the form.
 *
 * This is used by both Content and Term generation.
 *
 * @param string $items
 *   The name of the things that are being generated - 'nodes' or 'terms'.
 *
 * @return array
 *   The language details section of the form.
 */
  protected function getLanguageForm($items) {
    // We always need a language, even if the language module is not installed.
    $options = [];
    $languages = $this->languageManager->getLanguages(LanguageInterface::STATE_CONFIGURABLE);
    foreach ($languages as $langcode => $language) {
      $options[$langcode] = $language->getName();
    }

    $language_module_exists = $this->moduleHandler->moduleExists('language');
    $translation_module_exists = $this->moduleHandler->moduleExists('content_translation');

    $form['language'] = [
      '#type' => 'details',
      '#title' => $this->t('Language'),
      '#open' => $language_module_exists,
    ];
    $form['language'][SeederConstants::ORIGINAL_LANGUAGES] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Select the primary language(s) for @items', ['@items' => $items]),
      '#description' => $language_module_exists ? '' : $this->t('Disabled - requires Language module'),
      '#options' => $options,
      '#default_value' => [
        $this->languageManager->getDefaultLanguage()->getId(),
      ],
      '#disabled' => !$language_module_exists,
    ];
    $form['language'][SeederConstants::TRANSLATION_LANGUAGES] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Select the language(s) for translated @items', ['@items' => $items]),
      '#description' => $translation_module_exists ? $this->t('Translated @items will be created for each language selected.', ['@items' => $items]) : $this->t('Disabled - requires Content Translation module.'),
      '#options' => $options,
      '#disabled' => !$translation_module_exists,
    ];
    return $form;
  }

  /**
   * Return a language code.
   *
   * @param array $add_language
   *   Optional array of language codes from which to select one at random.
   *   If empty then return the site's default language.
   *
   * @return string
   *   The language code to use.
   */
  protected function getLangcode(array $add_language): string {
    if (empty($add_language)) {
      return $this->languageManager->getDefaultLanguage()->getId();
    }
    return $add_language[array_rand($add_language)];
  }

}
