<?php

namespace Drupal\seeder\Plugin\DevelGenerate;

use Drupal\Core\Form\FormStateInterface;
use Drupal\devel_generate\Plugin\DevelGenerate\UserDevelGenerate;
use Drupal\seeder\SeederConstants;
use Drupal\seeder\SeederDevelGenerateTrait;

/**
 * Class SeederUserDevelGenerate.
 *
 * @package Drupal\seeder\Plugin\DevelGenerate
 */
class SeederUserEntity extends UserDevelGenerate {

  /**
   * {@inheritdoc}
   */
  protected function getEntityTypeId() {
    return 'user';
  }

}
