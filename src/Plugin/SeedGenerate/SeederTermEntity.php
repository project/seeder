<?php

namespace Drupal\seeder\Plugin\DevelGenerate;

use Drupal\devel_generate\Plugin\DevelGenerate\SeederEntityBase;

/**
 * Class SeederTermEntityDevelGenerate.
 *
 * @package Drupal\seeder\Plugin\DevelGenerate
 */
class SeederTermEntity extends SeederEntityBase {

  /**
   * {@inheritdoc}
   */
  protected function getEntityTypeId(): string {
    return 'taxonomy_term';
  }

}
