<?php

namespace Drupal\seeder\Plugin\SeedGenerate\Deriver;

use Drupal\ctools\Plugin\Deriver\EntityDeriverBase;

/**
 * Class SeederEntityDeriver
 */
class SeederEntityDeriver extends EntityDeriverBase {

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    foreach ($this->entityTypeManager->getDefinitions() as $entityType) {
      $this->derivatives[$entityType->id()] = [
        'label' => $entityType->getLabel(),
        'description' => '',
        'entity_type' => $entityType->id()
      ];
    }

    return $this->derivatives;
  }

}
