<?php

namespace Drupal\seeder;

use Drupal\Component\Datetime\Time;
use Drupal\content_translation\ContentTranslationManagerInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Component\Utility\Random;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Exception;
use Faker\Factory;
use ReflectionClass;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;

/**
 * Class SeederEntityGenerator
 *
 * @package Drupal\seeder
 */
class SeederEntityGenerator {

  use StringTranslationTrait;

  /**
   * @var \Drupal\seeder\SeederDataSamplerManager
   */
  protected $seederDataSamplerManager;

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * @var \Drupal\Component\Utility\Random
   */
  protected $random;

  /**
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * Provides system time.
   *
   * @var \Drupal\Component\Datetime\Time
   */
  protected $time;

  /**
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The content translation manager.
   *
   * @var \Drupal\content_translation\ContentTranslationManagerInterface
   */
  protected $contentTranslationManager;

  protected $profileId;

  protected $locale;

  protected $entityTitleSampler;

  /**
   * SeederEntityGenerator constructor.
   *
   * @param \Drupal\seeder\SeederDataSamplerManager $seederDataSamplerManager
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   * @param \Drupal\content_translation\ContentTranslationManagerInterface $contentTranslationManager
   * @param \Drupal\Core\Language\LanguageManagerInterface $languageManager
   * @param \Drupal\Component\Datetime\Time $time
   * @param \Drupal\Core\Database\Connection $database
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   */
  public function __construct(
    SeederDataSamplerManager $seederDataSamplerManager,
    EntityTypeManagerInterface $entityTypeManager,
    ContentTranslationManagerInterface $contentTranslationManager,
    LanguageManagerInterface $languageManager,
    Time $time,
    Connection $database,
    MessengerInterface $messenger
  ) {
    $this->seederDataSamplerManager = $seederDataSamplerManager;
    $this->entityTypeManager = $entityTypeManager;
    $this->contentTranslationManager = $contentTranslationManager;
    $this->languageManager = $languageManager;
    $this->time = $time;
    $this->database = $database;
    $this->messenger = $messenger;
  }

  /**
   * @return \Drupal\seeder\SeederDataSamplerManager
   */
  public function getSeederDataSamplerManager(): SeederDataSamplerManager {
    return $this->seederDataSamplerManager;
  }

  /**
   * @param $entityTypeId
   * @param $options
   * @param $context
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function createEntitiesOperation($entityTypeId, $options, &$context): void {
    $max = 1;
    $limit = $options[SeederConstants::LIMIT];
    $offset = (!empty($context['sandbox']['offset'])) ?
      $context['sandbox']['offset'] : 0;

    $this->createEntity($entityTypeId, $options);

    // Redefine offset value.
    $context['sandbox']['offset'] = $offset + $max;

    // Set current step as unfinished until offset is greater than total.
    $context['finished'] = 0;
    if ($context['sandbox']['offset'] >= $limit) {
      $context['finished'] = 1;
    }

    // Setup info message to notify about current progress.
    $context['message'] = t(
      'Generated @consumed entities of @limit from available articles',
      [
        '@consumed' => $context['sandbox']['offset'],
        '@total' => $limit,
      ]
    );
  }

  /**
   * @param $entityTypeId
   * @param $options
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function cleanUpEntitiesOperation($entityTypeId, $options): void {
    $entityStorage = $this->entityTypeManager->getStorage($entityTypeId);
    $ids = $this->entityTypeManager->getStorage($entityTypeId)->getQuery()
      ->condition('type', $options[SeederConstants::BUNDLES], 'IN')
      ->execute();

    if (!empty($ids)) {
      $entities = $entityStorage->loadMultiple($ids);
      $entityStorage->delete($entities);
      $this->setMessage($this->t('Deleted %count entities.', ['%count' => count($ids)]));
    }
  }

  /**
   * Final operation to define message after executed all batch operations.
   *
   * @param bool $success
   * @param array $results
   */
  public function finishProcess($success, $results): void {
    // Setup final message after process is done.
    $message = ($success) ?
      t('Entity seeding process of @count was completed.',
        ['@count' => $results]) :
      t('Finished with an error.');
    $this->setMessage($message);
  }

  /**
   * Set a message for either drush or the web interface.
   *
   * @param string $msg
   *   The message to display.
   * @param string $type
   *   (optional) The message type, as defined in MessengerInterface. Defaults
   *   to MessengerInterface::TYPE_STATUS.
   */
  protected function setMessage($msg, $type = MessengerInterface::TYPE_STATUS): void {
    $this->messenger->addMessage($msg, $type);
  }

  /**
   * Create one node. Used by both batch and non-batch code branches.
   *
   * @param $entityTypeId
   * @param array $options
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Exception
   */
  public function createEntity($entityTypeId, $options): void {

    if ($options[SeederConstants::LOCALE] === '_none_') {
      $options[SeederConstants::LOCALE] = 'en_US';
    }

    if (!isset($options[SeederConstants::TIME_RANGE])) {
      $options[SeederConstants::TIME_RANGE] = 0;
    }

    $this->setLocale($options[SeederConstants::LOCALE]);
    $this->setProfileID($options[SeederConstants::PROFILE]);
    $this->setEntityTitleSampler($options[SeederConstants::ENTITY_TITLE]);

    $bundle = array_rand($options[SeederConstants::BUNDLES]);
    $uid = $this->getRandomUid();
    $entityStorage = $this->entityTypeManager->getStorage($entityTypeId);
    $entityType = $entityStorage->getEntityType();

    /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
    $entity = $entityStorage->create([
      'uid' => $uid,
      'revision' => random_int(0, 1),
      'created' => $this->time->getRequestTime() - random_int(0, $options[SeederConstants::TIME_RANGE]),
    ]);

    if ($entityType->hasKey('bundle')) {
      $entity->set($entityType->getKey('bundle'), $bundle);
    }

    if ($entityType->hasKey('revision')) {
      $entity->set($entityType->getKey('revision'), random_int(0, 1));
    }

    if ($entityType->hasKey('status')) {
      $entity->set($entityType->getKey('status'), TRUE);
    }

    if ($entityType->hasKey('label')) {
      $this->setLabel($entity, $entityType->getKey('label'), $options[SeederConstants::NAME_LENGTH]);
    }

    if ($entityType->hasKey('langcode')) {
      $entity->set($entityType->getKey('langcode'), $this->getLangCode($options[SeederConstants::ORIGINAL_LANGUAGES]));
    }

    try {
      $this->populateFields($entity);
      $entity->save();
    } catch (Exception $e) {
      // Do nothing.
    }

    if (is_null($this->contentTranslationManager)) {
      return;
    }

    if (!$entity->isTranslatable()) {
      return;
    }

    $langCode = $entity->language()->getId();
    if ($langCode === LanguageInterface::LANGCODE_NOT_SPECIFIED || $langCode === LanguageInterface::LANGCODE_NOT_APPLICABLE) {
      return;
    }

    if (!$this->contentTranslationManager->isEnabled($entity->getEntityTypeId(), $entity->bundle())) {
      return;
    }

    // Translate node to each target language.
    $skip_languages = [
      LanguageInterface::LANGCODE_NOT_SPECIFIED,
      LanguageInterface::LANGCODE_NOT_APPLICABLE,
      $entity->language()->getId(),
    ];

    foreach ($options[SeederConstants::TRANSLATION_LANGUAGES] as $translationLangCode) {
      if (in_array($translationLangCode, $skip_languages, TRUE)) {
        continue;
      }

      $translation = $entity->addTranslation($translationLangCode);

      if ($entityType->hasKey('label')) {
        $labelFieldName = $entityType->getKey('label');
        $translation->set($labelFieldName, sprintf('%s (%s)', $entity->get($labelFieldName), $langCode));
      }

      $this->populateFields($translation);
      $translation->save();
    }
  }

  /**
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   * @param string $labelFieldName
   * @param int $nameLength
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Exception
   */
  public function setLabel(ContentEntityInterface $entity, string $labelFieldName, int $nameLength): void {
    $entityTitleSampler = $this->getEntityTitleSampler();
    if ($entityTitleSampler === NULL || $entityTitleSampler === '_none_') {
      $entity->set($labelFieldName, $this->getRandom()->sentences(random_int(1, $nameLength), TRUE));
      return;
    }

    $labelFieldDefinition = $entity->get($labelFieldName)->getFieldDefinition();

    /** @var \Drupal\seeder\SeederDataSamplerInterface $seeder_sampler */
    $seeder_sampler = $this->getSeederDataSamplerManager()
      ->createInstance($entityTitleSampler);
    $entity->set($labelFieldName, $seeder_sampler->setLocale($this->getFakerLangCode($entity->language()->getId()))
      ->generateSeederValue($labelFieldDefinition));
  }

  /**
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException|\Drupal\Core\TypedData\Exception\ReadOnlyException
   * @throws \Exception
   */
  public function populateFields(ContentEntityInterface $entity): void {
    $seederProfile = $this->getProfile();

    if (!$seederProfile) {
      return;
    }

    $seederProfileDataSamplers = $seederProfile->getDataSamplers();

    /** @var \Drupal\field\FieldConfigInterface[] $instances */
    $instances = $this->getEntityTypeManager()
      ->getStorage('field_config')
      ->loadByProperties([
        'entity_type' => $entity->getEntityType()->id(),
        'bundle' => $entity->bundle(),
      ]);

    foreach ($instances as $instance) {
      $fieldStorage = $instance->getFieldStorageDefinition();
      $max = $cardinality = $fieldStorage->getCardinality();
      if ($cardinality === FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED) {
        $max = random_int(1, 3);
      }
      $fieldName = $fieldStorage->getName();
      $field = $entity->get($fieldName);
      $fieldDefinition = $field->getFieldDefinition();
      $fieldDefinitionType = $fieldDefinition->getType();

      // Match field type id with sampler.
      if (isset($seederProfileDataSamplers[$fieldDefinitionType])) {
        $seederSamplerId = $seederProfileDataSamplers[$fieldDefinitionType];
        /** @var \Drupal\seeder\SeederDataSamplerInterface $seederSampler */
        $seederSampler = $this->getSeederDataSamplerManager()
          ->createInstance($seederSamplerId);
        $values = [];
        for ($delta = 0; $delta < $max; $delta++) {
          $values[$delta] = $seederSampler->setLocale($this->getFakerLangCode($entity->language()->getId()))
            ->setFieldDefinition($fieldDefinition)
            ->generateSeederValue($fieldDefinition);
        }
        $field->setValue($values);
        continue;
      }

      $field->generateSampleItems($max);
    }
  }

  /**
   * @param $profileId
   *
   * @return mixed
   */
  public function setProfileId($profileId) {
    $this->profileId = $profileId;
    return $this;
  }

  /**
   * @return mixed
   */
  public function getProfileId() {
    return $this->profileId;
  }

  public function setLocale($locale): SeederEntityGenerator {
    $this->locale = $locale;
    return $this;
  }

  /**
   * @return mixed
   */
  public function getLocale() {
    return $this->locale;
  }

  public function setEntityTitleSampler($entityTitleSampler): SeederEntityGenerator {
    $this->entityTitleSampler = $entityTitleSampler;
    return $this;
  }

  /**
   * @return mixed
   */
  public function getEntityTitleSampler() {
    return $this->entityTitleSampler;
  }

  /**
   * @return \Drupal\Core\Entity\EntityInterface|null
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getProfile(): ?SeederProfileInterface {
    return $this->getEntityTypeManager()
      ->getStorage('seeder_profile')
      ->load($this->getProfileId());
  }

  /**
   * @return \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  public function getEntityTypeManager(): EntityTypeManagerInterface {
    return $this->entityTypeManager;
  }

  /**
   * @return \Drupal\Core\Language\LanguageManagerInterface
   */
  public function getLanguageManager(): LanguageManagerInterface {
    return $this->languageManager;
  }

  /**
   * Determine language based on $results.
   *
   * @param array $languages
   *
   * @return string
   */
  public function getLangCode(array $languages): string {
    if (isset($languages)) {
      $langCodes = $languages;
      $langCode = $langCodes[array_rand($langCodes)];
    }
    else {
      $langCode = $this->languageManager->getDefaultLanguage()->getId();
    }
    return $langCode;
  }

  /**
   * Determine language based on $results.
   *
   * @param string $langCode
   *
   * @return string
   */
  public function getFakerLangCode(string $langCode): string {
    $reflector = new ReflectionClass(Factory::class);
    $path = pathinfo($reflector->getFileName(), PATHINFO_DIRNAME) . '/Provider';

    $finder = new Finder();
    $finder->directories()->name(sprintf('%s_*', $langCode))->in($path)->depth('== 0');

    if (!$finder->hasResults()) {
      return $langCode;
    }

    return pathinfo(current($finder), PATHINFO_BASENAME);
  }

  /**
   * Returns the random data generator.
   *
   * @return \Drupal\Component\Utility\Random
   *   The random data generator.
   */
  protected function getRandom(): Random {
    if (!$this->random) {
      $this->random = new Random();
    }
    return $this->random;
  }

  /**
   * Return random uid.
   *
   * @return int
   */
  protected function getRandomUid(): int {
    $users = [];
    $result = $this->database->queryRange('SELECT uid FROM {users}', 0, 50);
    foreach ($result as $record) {
      $users[] = $record->uid;
    }

    return $users[array_rand($users)];
  }

}
