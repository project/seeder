<?php

namespace Drupal\seeder;

use Drupal\Core\Field\FieldDefinitionInterface;

/**
 * Interface SeederDataSamplerInterface.
 *
 * @package Drupal\seeder
 */
interface SeederDataSamplerInterface {

  /**
   * Get relevant field type ids.
   */
  public function getFieldTypeIds();

  /**
   * Generate sample data with Seeder.
   *
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   Field definition.
   */
  public function generateSeederValue(FieldDefinitionInterface $field_definition = NULL);

  /**
   * @param \Drupal\Core\Field\FieldDefinitionInterface $fieldDefinition
   *
   * @return $this
   */
  public function setFieldDefinition(FieldDefinitionInterface $fieldDefinition): SeederDataSamplerInterface;

  /**
   * @param string $locale
   *
   * @return $this
   */
  public function setLocale($locale): SeederDataSamplerInterface;
}
