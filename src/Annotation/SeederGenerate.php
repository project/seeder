<?php

namespace Drupal\seeder\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a SeederGenerate annotation object.
 *
 * SeederGenerate handle the bulk creation of entites.
 *
 * @Annotation
 *
 * @see \Drupal\seeder\SeederGeneratePluginManager
 * @see \Drupal\seeder\SeederGenerateBaseInterface
 */
class SeederGenerate extends Plugin {
  /**
   * The human-readable name of the SeederGenerate type.
   *
   * @ingroup plugin_translatable
   *
   * @var \Drupal\Core\Annotation\Translation
   */
  public $label;

  /**
   * A short description of the SeederGenerate type.
   *
   * @ingroup plugin_translatable
   *
   * @var \Drupal\Core\Annotation\Translation
   */
  public $description;

  /**
   * Id of the entity type.
   *
   * @var string
   */
  public $entity_type;

  /**
   * The name of the SeederGenerate class.
   *
   * This is not provided manually, it will be added by the discovery mechanism.
   *
   * @var string
   */
  public $class;

  /**
   * An array whose keys are the names of the settings available to the
   * SeederGenerate settingsForm, and whose values are the default values for those settings.
   *
   * @var array
   */
  public $settings = [];

  /**
   * An array whose keys are the settings available to the
   * SeederGenerate drush command: "suffix", "alias", "options" and "args".
   *
   * @var array
   */
  public $drushSettings = [];

  /**
   * Modules that should be enabled in order to make the plugin discoverable.
   *
   * @var array
   */
  public $dependencies = [];

}
