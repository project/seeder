<?php

namespace Drupal\seeder\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\seeder\SeederProfileInterface;

/**
 * Defines the Seeder Profile entity.
 *
 * @ConfigEntityType(
 *   id = "seeder_profile",
 *   label = @Translation("Seeder Profile"),
 *   handlers = {
 *     "list_builder" = "Drupal\seeder\SeederProfileListBuilder",
 *     "form" = {
 *       "add" = "Drupal\seeder\Form\SeederProfileForm",
 *       "edit" = "Drupal\seeder\Form\SeederProfileForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm",
 *     }
 *   },
 *   config_prefix = "seeder_profile",
 *   admin_permission = "administer seeder profile configuration",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "bundles" = "bundles",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "bundles",
 *     "data_samplers",
 *   },
 *   links = {
 *     "edit-form" = "/admin/config/development/devel/seeder/{seeder_profile}",
 *     "delete-form" = "/admin/config/development/devel/seeder/{seeder_profile}/delete",
 *   }
 * )
 */
class SeederProfile extends ConfigEntityBase implements SeederProfileInterface {

  /**
   * The Seeder Profile ID.
   *
   * @var string
   */
  public $id;

  /**
   * The Seeder Profile label.
   *
   * @var string
   */
  public $label;

  /**
   * Allowed bundles of entity types.
   *
   * @var array
   */
  public $bundles;

  /**
   * The Seeder Profile data samplers.
   *
   * @var mixed
   */
  public $data_samplers;

  /**
   * {@inheritdoc}
   */
  public function getBundles(): array {
    return $this->bundles;
  }

  /**
   * {@inheritdoc}
   */
  public function setBundles(array $bundles): SeederProfileInterface {
    $this->bundles = $bundles;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getDataSamplers(): array {
    return $this->data_samplers;
  }

  /**
   * {@inheritdoc}
   */
  public function setDataSamplers(array $data_samplers): SeederProfileInterface {
    $this->data_samplers = $data_samplers;
    return $this;
  }

}
