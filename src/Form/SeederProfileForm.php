<?php

namespace Drupal\seeder\Form;

use Drupal\Core\Entity\ContentEntityType;
use Drupal\Core\Entity\ContentEntityTypeInterface;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Field\FieldTypePluginManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\SubformState;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\search_api\IndexInterface;
use Drupal\seeder\SeederDataSamplerManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form for adding Seeder Profiles.
 */
class SeederProfileForm extends EntityForm {

  /**
   * Field Type Plugin Manager.
   *
   * @var \Drupal\Core\Field\FieldTypePluginManagerInterface
   */
  protected $fieldTypePluginManager;

  /**
   * Seeder Data Sampler Manager.
   *
   * @var \Drupal\seeder\SeederDataSamplerManager
   */
  protected $seederDataSamplerManager;

  /**
   * SeederProfileForm constructor.
   *
   * @param \Drupal\Core\Field\FieldTypePluginManagerInterface $fieldTypePluginManager
   * @param \Drupal\seeder\SeederDataSamplerManager $seederDataSamplerManager
   */
  public function __construct(FieldTypePluginManagerInterface $fieldTypePluginManager, SeederDataSamplerManager $seederDataSamplerManager) {
    $this->fieldTypePluginManager = $fieldTypePluginManager;
    $this->seederDataSamplerManager = $seederDataSamplerManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.field.field_type'),
      $container->get('seeder.data_sampler_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $this->entity->label(),
      '#description' => $this->t('Label for the Seeder Profile.'),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $this->entity->id(),
      '#machine_name' => [
        'exists' => [$this, 'exist'],
      ],
      '#disabled' => !$this->entity->isNew(),
    ];

    $form['entity_types'] = [
      '#type' => 'fieldset',
      '#tree' => true,
    ];

    foreach ($this->entityTypeManager->getDefinitions() as $entityType) {
      if (!$entityType instanceof ContentEntityTypeInterface) {
        continue;
      }

      $bundleInfo = \Drupal::service('entity_type.bundle.info')
        ->getBundleInfo($entityType->id());
      $bundles = [
        '#type' => 'checkboxes',
      ];
      foreach ($bundleInfo as $bundleId => $bundleData) {
        $bundles['#options'][$bundleId] = $bundleData['label'];
      }

      $form['entity_types'][$entityType->id()] = [
        '#type' => 'fieldset',
        '#title' => $entityType->getLabel(),
        'bundles' => $bundles,
      ];
    }


    $form['data_samplers'] = [
      '#type' => 'table',
      '#header' => [
        [
          'data' => $this->t('Field Type'),
        ],
        [
          'data' => $this->t('Field Type ID'),
        ],
        [
          'data' => $this->t('Data Sampler'),
        ],
      ],
      '#empty' => $this->t('No field types found.'),
    ];

    $field_type_definitions = $this->fieldTypePluginManager->getDefinitions();
    $field_type_sampler_definitions = $this->seederDataSamplerManager->getDefinitions();
    $rows = [];
    foreach ($field_type_definitions as $field_type => $field_type_definition) {
      $seeder_sampler_options = [];
      foreach ($field_type_sampler_definitions as $seeder_sampler_id => $field_type_sampler_definition) {
        // Match field type id with sampler.
        if (in_array($field_type, $field_type_sampler_definition['field_type_ids'], TRUE)) {
          $seeder_sampler_options[$seeder_sampler_id] = $field_type_sampler_definition['label'];
        }
      }
      $rows[$field_type] = [
        [
          'data' => [
            '#markup' => $field_type_definition['label'],
          ],
        ],
        [
          'data' => [
            '#markup' => $field_type_definition['id'],
          ],
        ],
        [
          'data' => [
            '#type' => 'select',
            '#options' => [
              '_none_' => $this->t('Default'),
            ] + $seeder_sampler_options,
            '#default_value' => $this->entity->getDataSamplers()[$field_type] ?? NULL,
            '#parents' => ['data_samplers', $field_type_definition['id']],
          ],
        ],
      ];
    }

    $form['data_samplers'] = array_merge($form['data_samplers'], $rows);

    return $form;
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\Entity\EntityMalformedException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function save(array $form, FormStateInterface $form_state) {

    $data_samplers = $form_state->getValue('data_samplers', NULL);
    $data_samplers = array_filter($data_samplers, static function ($value) {
      return $value !== '_none_';
    });

    $entity = $this->getEntity();
    $entity->setDataSamplers($data_samplers);

    $edit_link = $entity->toLink($this->t('Edit'), 'edit-form')->toString();

    if ($entity->save() === SAVED_NEW) {
      $this->messenger()->addMessage($this->t('Created new Seeder Profile.'));
      $this->logger('seeder')->notice('Created new Seeder Profile %name.', ['%name' => $entity->label(), 'link' => $edit_link]);
    }
    else {
      $this->messenger()->addMessage($this->t('Updated Seeder Profile.'));
      $this->logger('seeder')->notice('Updated Seeder Profile %name.', ['%name' => $entity->label(), 'link' => $edit_link]);
    }
    $form_state->setRedirect('entity.seeder_profile.collection');
  }

  /**
   * Helper function to check whether the configuration entity exists.
   *
   * @param string|int $id
   *
   * @return bool
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function exist($id): bool {
    return (bool) $this->entityTypeManager
      ->getStorage('seeder_profile')
      ->getQuery()
      ->condition('id', $id)
      ->execute();
  }
}
