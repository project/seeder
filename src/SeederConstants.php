<?php

namespace Drupal\seeder;

/**
 * Class SeederConstants.
 */
abstract class SeederConstants {
  public const CLEAN = 'clean';

  public const LIMIT = 'limit';

  /**
   * original language form key.
   */
  public const ORIGINAL_LANGUAGES = 'original_languages';

  /**
   * translation languages form key.
   */
  public const TRANSLATION_LANGUAGES = 'translation_languages';

  /**
   * profile form key.
   */
  public const PROFILE = 'profile';

  /**
   * locale form key.
   */
  public const LOCALE = 'locale';

  /**
   * time_range form key.
   */
  public const TIME_RANGE = 'time_range';

  /**
   * bundles form key.
   */
  public const BUNDLES = 'bundles';

  /**
   * name_length form key.
   */
  public const NAME_LENGTH = 'name_length';

  /**
   * entity title form key.
   */
  public const ENTITY_TITLE = 'entity_title';

  public const LOCALES = [
    'ar_JO',
    'ar_SA',
    'at_AT',
    'bg_BG',
    'bn_BD',
    'cs_CZ',
    'da_DK',
    'de_AT',
    'de_CH',
    'de_DE',
    'el_CY',
    'el_GR',
    'en_AU',
    'en_CA',
    'en_GB',
    'en_HK',
    'en_IN',
    'en_NG',
    'en_NZ',
    'en_PH',
    'en_SG',
    'en_UG',
    'en_US',
    'en_ZA',
    'es_AR',
    'es_ES',
    'es_PE',
    'es_VE',
    'et_EE',
    'fa_IR',
    'fi_FI',
    'fr_BE',
    'fr_CA',
    'fr_CH',
    'fr_FR',
    'he_IL',
    'hr_HR',
    'hu_HU',
    'hy_AM',
    'id_ID',
    'is_IS',
    'it_CH',
    'it_IT',
    'ja_JP',
    'ka_GE',
    'kk_KZ',
    'ko_KR',
    'lt_LT',
    'lv_LV',
    'me_ME',
    'mn_MN',
    'ms_MY',
    'nb_NO',
    'ne_NP',
    'nl_BE',
    'nl_NL',
    'pl_PL',
    'pt_BR',
    'pt_PT',
    'ro_MD',
    'ro_RO',
    'ru_RU',
    'sk_SK',
    'sl_SI',
    'sr_Cyrl_RS',
    'sr_Latn_RS',
    'sr_RS',
    'sv_SE',
    'th_TH',
    'tr_TR',
    'uk_UA',
    'vi_VN',
    'zh_CN',
    'zh_TW',
  ];
}
