INTRODUCTION
------------

The Seeder module allows you to generate fake data using the PHP Faker library.

Read more about [Faker](https://github.com/fzaninotto/Faker).

REQUIREMENTS
------------

Optionally install Devel Generate which will allow you to generate content using Faker.

INSTALLATION
------------

Install as usual, see [Installing contributed modules
](https://drupal.org/node/895232) for further information.

CONFIGURATION
-------------

1. Navigate to settings form through `Admin > Configuration > Development > Seeder`

   or directly at path `/admin/config/development/seeder-profile`
